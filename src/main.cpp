#include <mbed.h>

DigitalOut myled(LED1);

int main() {
    while(1) {
        myled = 1;
        wait_us(1e6);
        myled = 0;
        wait_us(1e6);
    }
}
